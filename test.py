import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


options = webdriver.ChromeOptions()
capabilities = options.to_capabilities()
driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=capabilities)

driver.get("http://google.com")
time.sleep(5)
driver.save_screenshot('docker_image_1.png')
elem = driver.find_element_by_xpath('/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input')
elem.send_keys("MIPT")
elem.send_keys(Keys.RETURN)
time.sleep(5)
driver.save_screenshot('docker_image_2.png')
driver.quit()